<!doctype html>
<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Alquileres</title>
    <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
    <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->
    <style type="text/css">
        body {
            margin: 0;
            font-family: "Helvetica Neue", Helvetica, Arial, "sans-serif";
        }

        body, table, td, p, a, li, blockquote {
            -webkit-text-size-adjust: none !important;
            font-family: sans-serif;
            font-style: normal;
            font-weight: 400;
        }

        button {
            width: 90%;
        }

        @media screen and (max-width: 500px) {
            /*styling for objects with screen size less than 500px; */
            body, table, td, p, a, li, blockquote {
                -webkit-text-size-adjust: none !important;
                font-family: sans-serif;
            }

            table {
                /* All tables are 100% width */
                width: 100%;
            }

            .footer {
                /* Footer has 2 columns each of 48% width */
                height: auto !important;
                max-width: 48% !important;
                width: 48% !important;
            }

            table.responsiveImage {
                /* Container for images in catalog */
                height: auto !important;
                max-width: 30% !important;
                width: 30% !important;
            }

            table.responsiveContent {
                /* Content that accompanies the content in the catalog */
                height: auto !important;
                max-width: 66% !important;
                width: 66% !important;
            }

            .top {
                /* Each Columnar table in the header */
                height: auto !important;
                max-width: 48% !important;
                width: 48% !important;
            }

            .catalog {
                margin-left: 0% !important;
                text-align: center;
            }

            .full {
                width: 90% !important;
            }

            .full.main {
                padding-right: 20px !important;
                padding-left: 20px !important;
            }
        }

        @media screen and (max-width: 480px) {
            /*styling for objects with screen size less than 480px; */
            body, table, td, p, a, li, blockquote {
                -webkit-text-size-adjust: none !important;
                font-family: sans-serif;
            }

            table {
                /* All tables are 100% width */
                width: 100% !important;
                border-style: none !important;
            }

            .footer {
                /* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
                height: auto !important;
                max-width: 96% !important;
                width: 96% !important;
            }

            .table.responsiveImage {
                /* Container for each image now specifying full width */
                height: auto !important;
                max-width: 96% !important;
                width: 96% !important;
            }

            .table.responsiveContent {
                /* Content in catalog  occupying full width of cell */
                height: auto !important;
                max-width: 96% !important;
                width: 96% !important;
            }

            .top {
                /* Header columns occupying full width */
                height: auto !important;
                max-width: 100% !important;
                width: 100% !important;
            }

            .catalog {
                margin-left: 0% !important;
            }

            button {
                width: 99% !important;
            }
        }
    </style>
</head>
<body yahoo="yahoo">
<?php
$name = $_GET['nombre'];
$email = $_GET['email'];
$seguro = $_GET['seguro'];
if ($seguro=='otros'){
    $seguro='caución';
}
if ($seguro=='ambiental'){
    $seguro='Caución Ambiental';
}
if ($seguro=='obra'){
    $seguro='caución de obra';
}
$cotizacion_price = $_GET['cotizacion'];
$date = date('Y-m-d');
$cotizacion = substr($cotizacion_price, 0, strpos($cotizacion_price, "."));
$cotizacion = str_replace('$', "", $cotizacion_price);
$cotizacion = str_replace(',', "", $cotizacion);
$cotizacion = str_replace('.', "", $cotizacion);
$position = true;
?>
<table width="100%" cellspacing="0" cellpadding="0" style="background-color: #eaeaea">
    <tbody>
    <tr>
        <!-- HTML Spacer row -->
        <td style="font-size: 0; line-height: 0;" height="20">
            <table width="100%" align="left" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="font-size: 0; line-height: 0;" height="40">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="550" class="full" align="center" cellpadding="0" cellspacing="0"
                   style="background-color: white;">
                <!-- Main Wrapper Table with initial width set to 60opx -->
                <tbody>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;background-color: #003682;" height="8"></td>
                </tr>
                <tr>
                    <!-- HTML IMAGE SPACER -->
                </tr>
                </tbody>
            </table>
            <table width="550" class="full main" align="center" cellpadding="0" cellspacing="0"
                   style="padding-top: 30px; background-color: white; padding-right: 40px; padding-left:40px;">
                <tbody>
                <tr>
                    <td style="font-size: 0; text-align: center; line-height: 0;" height="20">
                        <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/header.png"
                             alt="" style="margin: 0 auto; text-align: center; max-width: 100%;"/>
                    </td>
                </tr>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="100%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="40">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- Introduction area -->
                    <td>
                        <table width="100%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <!-- row container for TITLE/EMAIL THEME -->
                                <td align="center" style="
                                       font-size: 26px;
                                       font-weight: bold;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 30px;
                                       letter-spacing: normal;
                                       text-align: center;
                                       color: #4a4a4a;
                                       padding-top: 30px;
                                       ">
                                    <?php if ($cotizacion < 30000) { ?>
                                        ¡Estamos más cerca de darte la cotización de tu seguro de <?php echo $seguro; ?>!

                                    <?php }else{?>
                                    ¡Ya tenemos la cotizacion para tu seguro de <?php echo $seguro; ?>!
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <!-- Row container for Intro/ Description -->
                                <td align="left" style="
                                       font-size: 16px;
                                       font-weight: normal;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 1.69;
                                       letter-spacing: normal;
                                       text-align: center;
                                       color: #4a4a4a;
                                       padding-top: 20px;
                                       ">
                                    <?php if ($cotizacion < 30000) {
                                        unset($_GET['cotizacion']);
                                        $position = false;
                                        if ($seguro == 'alquiler') { ?>
                                            Hola <?php echo $name; ?>, soy Martina de Foms Seguros. Para darte la cotización puede ser que
                                            necesitemos que presentes que una tercero salga como garante con su recibo de sueldo y/o que
                                            nos indiques si sos propietario de un automóvil, moto o inmueble. Preguntame lo que necesites,
                                            estoy a tu disposición.
                                        <?php } else { ?>
                                            Hola <?php echo $name; ?>, soy Maximiliano de Foms Seguros. Nos falta un poco más de información
                                            para poder darte la cotización. En breve nos pondremos en contacto con vos. También podes
                                            llamarnos al 6009 5100.

                                        <?php }

                                        ?>

                                    <?php } else {
                                        if ($seguro == 'alquiler') {  ?>
                                        Hola <?php if ($name != '') {
                                            echo $name;
                                        } ?>, soy Martina de Foms Seguros. Te comparto la cotización que realizaste
                                        en nuestro sitio y me pongo a disposición para avanzar con la emisión de tu seguro.
                                            <?php }else{ ?>
                                            Hola <?php if ($name != '') {
                                                echo $name;
                                            } ?>, soy Maximiliano de Foms Seguros. Te comparto la cotización que realizaste
                                            en nuestro sitio y me pongo a disposición para avanzar con la emisión de tu seguro.
                                            <?php } ?>
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="10">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="40">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;">
                        <table width="96%" align="center" cellpadding="0" cellspacing="0">
                            <?php if ($cotizacion > 30000) { ?>

                                <tr>
                                    <td style="
                                       font-size: 14px;
                                       font-weight: bold;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 1.93;
                                       letter-spacing: normal;
                                       text-align: center;
                                       color: #4a4a4a;
                                       padding-bottom:20px;"
                                    >
                                        TU COTIZACIÓN ESTIMADA
                                    </td>
                                </tr>
                                <tr>
                                    <td style="
                                       font-size: 38px;
                                       font-weight: bold;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 0.47;
                                       letter-spacing: normal;
                                       text-align: center;
                                       color: #003682;">
                                        <?php echo $cotizacion_price; ?>*
                                    </td>
                                </tr>
                                <tr>
                                <tr>
                                    <td style="font-size: 10px;
                                       font-weight: normal;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 1.56;
                                       letter-spacing: normal;
                                       text-align: center;
                                       color: #4a4a4a;
                                       padding-top: 10px;">
                                        <div style="max-width: 250px; margin: 0 auto;">
                                            <?php if ($seguro == 'alquiler') {  ?>
                                                * Para cotizar se toma como referencia un incremento del 15% semestral
                                                sobre el valor declarado del alquiler y las expensas mensuales.
                                            <?php } ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td style="font-size: 10px;
                                       font-weight: normal;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 1.56;
                                       letter-spacing: normal;
                                       text-align: center;
                                       color: #4a4a4a;
                                       padding-top: 10px;">
                                    <div style="max-width: 250px; margin: 0 auto;">

                                        <?php if ($position) {
                                            if ($seguro == 'alquiler' && $cotizacion < 30000) {
                                                echo "En breve un representante de nuestra compañía se pondrá en contacto con vos.";
                                            } else {
                                                if ($seguro == 'obra' || $seguro == 'servicios') {
                                                    echo "Prima Trimestral (no incluye impuestos)";
                                                } else if ($seguro == 'sociedades') {
                                                    echo "Por socio asegurado";
                                                }
                                            }
                                        } ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="60">
                        <table width="100%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="40">
                                    <div style="border: solid 1px #e6e6e6; margin-top: 10px;"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="100%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style=" font-size: 19px;
                                       font-weight: bold;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 1.42;
                                       letter-spacing: normal;
                                       text-align: center;
                                       color: #003682;" height="40">
                                    Información que completaste
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" align="center" width="100%" style="margin-top: 10px"
                               class="catalog">
                            <!-- Table for catalog -->
                            <tr>
                                <td>


                                    <table class="responsive-table" width="48%" cellspacing="0" cellpadding="0"
                                           align="left" style="margin: 0px 0px 0px 0px;">
                                        <!-- Table container for each image and description in catalog -->
                                        <tbody>
                                        <tr>
                                            <td>

                                                <table class="table.responsiveContent" width="100%" cellspacing="0"
                                                       cellpadding="0" align="left">
                                                    <!-- Table container for content -->
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size: 14px;
                                                               font-weight: normal;
                                                               font-style: normal;
                                                               font-stretch: normal;
                                                               letter-spacing: normal;
                                                               color: #4a4a4a;
                                                               line-height: 20px;">
                                                            <?php
                                                            $count = 0;
                                                            unset($_GET['cotizacion']);
                                                            foreach ($_GET as $key => $value) {
                                                                $count++;
                                                                if ($count % 2 != 0) {
                                                                    ?>

                                                                    <p><?php echo ucwords(str_replace('_', ' ', $key)); ?>
                                                                        <br>
                                                                        <strong><span><?php echo $value; ?></span></strong>
                                                                    </p>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table class="responsive-table" width="48%" cellspacing="0" cellpadding="0"
                                           align="left" style="margin: 0px 0px 10px 0px;">
                                        <!-- Table container for each image and description in catalog -->
                                        <tbody>
                                        <tr>
                                            <td>

                                                <table class="table.responsiveContent" width="100%" cellspacing="0"
                                                       cellpadding="0" align="left">
                                                    <!-- Table container for content -->
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size: 14px;
                                                               font-weight: normal;
                                                               font-style: normal;
                                                               font-stretch: normal;
                                                               letter-spacing: normal;
                                                               color: #4a4a4a;
                                                               line-height: 20px;">
                                                            <?php
                                                            $count = 0;
                                                            foreach ($_GET as $key => $value) {
                                                                $count++;
                                                                if ($count % 2 == 0) { ?>
                                                                    <p><?php echo ucwords(str_replace('_', ' ', $key)); ?>
                                                                        <br>
                                                                        <strong><span><?php echo $value; ?></span></strong>
                                                                    </p>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>


                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="40">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>

               <?php if ($seguro == 'alquiler') { ?>
                <tr>
                    <!-- HTML Spacer row -->
                    <td style="font-size: 0; line-height: 0;">
                        <table width="100%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style=" font-size: 19px;
                                       font-weight: bold;
                                       font-style: normal;
                                       font-stretch: normal;
                                       line-height: 1.42;
                                       letter-spacing: normal;
                                       text-align: center;
                                       color: #003682;" height="40">
                                    Para que tengas en cuenta
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 16px;
                              font-weight: normal;
                              font-style: normal;
                              font-stretch: normal;
                              line-height: 1.25;
                              letter-spacing: normal;
                              color: #4a4a4a;
                              text-align: left;">
                        <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/group.png"
                             width="58" height="58" alt="" align="left" style="margin-right: 20px; "/>
                        Podés pagarlo por depósito o transferencia bancaria, con tarjeta de crédito en un pago o en cuotas con MercadoPago.

                    </td>
                </tr>



                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 16px;
                              font-weight: normal;
                              font-style: normal;
                              font-stretch: normal;
                              line-height: 1.25;
                              letter-spacing: normal;
                              color: #4a4a4a;
                              text-align: left; vertical-align: middle; display: table-cell;">
                        <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/group-5.png" width="58"
                             height="58" alt="" align="left" style="margin-right: 20px;"/>
                        Puede ser que te solicitemos que un
                        familiar o amigo te salga de garante.
                    </td>
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 16px;
                              font-weight: normal;
                              font-style: normal;
                              font-stretch: normal;
                              line-height: 1.25;
                              letter-spacing: normal;
                              color: #4a4a4a;
                              text-align: left;">
                        <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/group-copy.png"
                             width="58" height="58" alt="" align="left"
                             style="margin-right: 20px; "/>
                        Cuando tengas la propiedad elegida, te vamos a pedir una copia preliminar del contrato de
                        alquiler.
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="20">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 16px;
                              font-weight: normal;
                              font-style: normal;
                              font-stretch: normal;
                              line-height: 1.25;
                              letter-spacing: normal;
                              color: #4a4a4a;
                              text-align: left;">
                        <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/group-13.png"
                             width="58" height="58" alt="" align="left"
                             style="margin-right: 20px; "/>
                        Una vez aprobada la cotización y luego de presentar la documentación, la poliza se emite en el
                        día.
                </tr>

                <?php } ?>
                <tr>
                    <!-- HTML spacer row -->
                    <td style="font-size: 0; line-height: 0;" height="40">
                        <table width="96%" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <!-- HTML spacer row -->
        <td style="font-size: 0; line-height: 0;" height="20">
            <table width="96%" align="left" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" align="center" width="500" style="" class="footer full">
                <!-- Table for catalog -->
                <tr>
                    <td>
                        <table class="responsive-table" width="32%" cellspacing="0" cellpadding="0"
                               align="left" style="margin: 10px 0px 10px 0px;">
                            <!-- Table container for each image and description in catalog -->
                            <tbody>
                            <tr>
                                <td>
                                    <table class="table.responsiveContent" width="100%" cellspacing="0"
                                           cellpadding="0" align="left">
                                        <!-- Table container for content -->
                                        <tbody>
                                        <tr>
                                            <td style="">
                                                <a href="tel:+54901160095100" style=" font-size: 16px;
                                                      font-weight: bold;
                                                      font-style: normal;
                                                      font-stretch: normal;
                                                      line-height: 1.25;
                                                      letter-spacing: normal;
                                                      color: #003682;
                                                      text-decoration: none;
                                                      ">
                                                    <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/phone.png"
                                                         alt="" align="left"
                                                         style="margin-right: 10px; margin-top: -4px; "/>
                                                    011 6009 5100
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="responsive-table" width="50%" cellspacing="0" cellpadding="0"
                               align="left" style="margin: 10px 0px 10px 0px;">
                            <!-- Table container for each image and description in catalog -->
                            <tbody>
                            <tr>
                                <td>
                                    <table class="table.responsiveContent" width="100%" cellspacing="0"
                                           cellpadding="0" align="left">
                                        <!-- Table container for content -->
                                        <tbody>
                                        <tr>
                                            <td style="">
                                                <a href="mailto:comercial@foms.com.ar" style=" font-size: 16px;
                                                      font-weight: bold;
                                                      font-style: normal;
                                                      font-stretch: normal;
                                                      line-height: 1.25;
                                                      letter-spacing: normal;
                                                      color: #003682;
                                                      text-decoration: none;
                                                      ">
                                                    <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/group-copy-3.png"
                                                         alt="" align="left"
                                                         style="margin-right: 10px; margin-top: 2px;"/>
                                                    comercial@foms.com.ar
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="responsive-table" width="15%" cellspacing="0" cellpadding="0"
                               align="left" style="margin: 10px 0px 10px 0px;">
                            <!-- Table container for each image and description in catalog -->
                            <tbody>
                            <tr>
                                <td>
                                    <table class="table.responsiveContent" width="100%" cellspacing="0"
                                           cellpadding="0" align="left">
                                        <!-- Table container for content -->
                                        <tbody>
                                        <tr>
                                            <td style="">
                                                <a href="https://www.foms.com.ar" style=" font-size: 16px;
                                                      font-weight: bold;
                                                      font-style: normal;
                                                      font-stretch: normal;
                                                      line-height: 1.25;
                                                      letter-spacing: normal;
                                                      color: #003682;
                                                      text-decoration: none;
                                                      ">
                                                    <img src="https://foms.com.ar/wp-content/themes/stockholm/emails/assets/group-copy-4.png"
                                                         alt="" align="left"
                                                         style="margin-right: 10px; margin-top: 2px;"/>
                                                    chat
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10px;
                           font-weight: normal;
                           font-style: normal;
                           font-stretch: normal;
                           line-height: normal;
                           letter-spacing: normal;
                           color: #4a4a4a;
                           text-align: center;
                           padding-top: 20px;padding-bottom: 20px;">
                        <?php  if ($seguro == 'alquiler'){ ?>
                        La presente cotización es de referencia, se considera un incremento semestral
                        del 15% para el alquiler y las expensas.
                        <?php } ?>

                        La información indicada en esta
                        página está sujeta a verificación comercial y crediticia de Foms Cia.
                        Argentina de Seguros SA y a la presentación de documentación respaldatoria.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
