<?php
/**
 * Template Name: Caución Form Subsidio
 *
 */
 ?>
<?php get_header(); ?>
<div id="contenido">
    <div id="url" class="d-none"><?php echo home_url(); ?></div>
    <div class="fondo-sub d-xs-none"></div>
    <div class="container">
        <div class="row bd-title justify-content-center">
            <div class="col-lg-6">
                <div class="cinta">
                    <h1>GARANTÍA BA</h1>
                    <h2>SEGURO DE CAUCIÓN</h2>
                </div>
            </div>
            <div class="col-lg-5 bg-light pad">
                <div class="counter text-center align-self-center">
                    <div class="counter-step active">
                        <div class="number" id="number-1">1</div>
                    </div>
                    <div class="counter-step st">
                        <div class="line"></div>
                        <div class="number" id="number-2">2</div>
                    </div>
                    <div class="counter-step st">
                        <div class="line"></div>
                        <div class="number" id="number-3">3</div>
                    </div>
                </div>
                <div id="alerta" class="alert alert-danger d-none" role="alert">
                    La información no fue enviada correctamente. por favor vuelve a completar el formulario.
                </div>
                <div class="pos">
                    <div class="rel">
                     <form id="contact-form" data-abide='ajax' data-toggle="validator" role="form">
                            <!--<div class="cotizacion"></div>-->
                                <fieldset id="step-1" class="step animated fadeInUp active" data-step="1">
                                    <h1>Cotizador online</h1>
                                    <p class="lead">Complete con sus datos</p>
                                    <div class="form-group d-none">
                                        <label>¿Tipo de seguro?</label>
                                        <input type="text" class="form-control" id="seguro" name="seguro"
                                               value="alquiler">
                                        <div class="invalid-feedback">
                                            Elije un tipo de seguro
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Nombre y apellido</label>
                                        <input type="text" class="form-control required" id="nombre" name="nombre"
                                               aria-describedby="emailHelp" required>
                                        <div class="invalid-feedback">
                                            Ingresa tu nombre
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="tel" class="form-control required" id="telefono" name="telefono"
                                               aria-describedby="emailHelp" required>
                                        <div class="invalid-feedback">
                                            Ingresa tu teléfono
                                        </div>
                                    </div>
                                    <div class="form-group" esto="Cosjj">
                                        <label>Email</label>
                                        <input type="email" class="form-control required" id="email"  name="email">
                                        <div class="invalid-feedback">
                                            Ingresa tu email
                                        </div>
                                    </div>

                                    <div class="form-group boton">
                                        <button class="siguiente qbutton mt-1 ">CONTINUAR <i
                                                    class="fas fa-arrow-right"></i>
                                        </button>
                                    </div>
                                </fieldset>
                                <fieldset id="step-2" class="step animated" data-step="2">
                                    <div id="alquileres" class="tipo d-none">
                                        <h1>Último paso</h1>
                                        <p class="lead">Ya falta poco para que te demos la cotización</p>
                                        <div class="form-group">
                                            <label>Ingresos mensuales del grupo familiar</label>
                                            <input class="form-control money" id="ingresos" name="ingresos">
                                            <div class="invalid-feedback">
                                                Completa el campo
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Cuit del inquilino</label>
                                            <input class="form-control cuit" id="cuitInquilino"  name="cuit_Inquilino">
                                            <div class="invalid-feedback">
                                                Ingresa el cuit correspondiente
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label>¿Cuánto vas a pagar por mes de alquiler?</label>
                                            <input class="form-control money" id="importe"
                                                   name="importe">
                                            <div class="invalid-feedback">
                                                Ingresa el importe
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>¿Deseas sumar las expensas a la cobertura?</label>
                                            <select class="form-control" name="incluye_Expensas" id="incExpensas">
                                                <option value="0">Seleccionar</option>
                                                <option value="si">Si</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>
                                        <div id="idExp" class="form-group d-none">
                                            <label>¿Cuánto vas a pagar de expensas por mes?</label>
                                            <input class="form-control money" id="expensas"
                                                   name="expensas">
                                            <div class="invalid-feedback">
                                                Ingresa el valor de expensas
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="checkbox" name="posee_Subsidio" class="form-control" id="subsidio"> Tildá esta opción si sos beneficiario del subsidio del 30% de GarantíaBA
                                        </div>
                                        <div class="form-group boton">
                                            <button  type="submit" class="qbutton mt-1 " id="submit">CONTINUAR <i class="fas fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="step-3" class="step animated" data-step="3">
                                    <div id="cotizaciones">
                                        <h1>La cotización estimada para tu seguro es de <span id="total"></span></h1>
                                        <h3>
                                            Te enviamos un correo con la información sobre este seguro. Si lo desea puede llamarnos al 011 6009 5100.
                                        </h3>
                                        <hr>
                                        <p>Esta cotización está sujeta a la verificación crediticia de FOMS Cia. Argentina de Seguros SA y
                                            a la presentación de documentación respaldatoria. Para recibir la tasa preferencial del 4% debe
                                            estar calificado por Garantía BA. Para ser beneficiario del descuento del 30% debe haber sido
                                            seleccionado y notificado por el IVC de la Ciudad de Buenos Aires.</p>
                                    </div>
                                    <div id="solicitud">
                                        <h1>Recibimos su solicitud, en breve un representante comercial se comunicará con UD.</h1>
                                        <h3>
                                            Cualquier duda nuestro teléfono de contacto es 011 6009 5100.
                                        </h3>
                                        <hr>
                                    </div>
                                    <a class="qbutton mt-1 d-none" id="reset" href="<?php echo home_url()  ?>/garantiaba">Volver a Cotizar</a>
                                </fieldset>
                                <input class="form-control" type="hidden" name="cotizacion" id="cotizacion" value=""/>
                                <input type="hidden" name="submitted" id="submitted" value="true"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row icons">
       <div class="col-12">
           <?php the_content(); ?>
       </div>
    </div>
</div>
<script type="text/javascript">

</script>
<?php get_footer(); ?>


