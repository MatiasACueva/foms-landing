<?php
/**
 * Template Name: Caución Form
 *
 */
 ?>
<?php get_header(); ?>
<div id="contenido">
    <div id="url" class="d-none"><?php echo home_url(); ?></div>
    <div class="fondo d-xs-none"></div>
    <div class="container">
        <div class="row bd-title justify-content-center">
            <div class="col-lg-6">
                <div class="cinta">
                    <h1>SEGURO DE CAUCIÓN</h1>
                    <h2>COBERTURA EN TODO EL PAÍS</h2>
                </div>
            </div>
            <div class="col-lg-5 bg-light pad">
                <div class="counter text-center align-self-center">
                    <div class="counter-step active">
                        <div class="number" id="number-1">1</div>
                    </div>
                    <div class="counter-step st">
                        <div class="line"></div>
                        <div class="number" id="number-2">2</div>
                    </div>
                    <div class="counter-step st">
                        <div class="line"></div>
                        <div class="number" id="number-3">3</div>
                    </div>
                </div>
                <div id="alerta" class="alert alert-danger d-none" role="alert">
                    La información no fue enviada correctamente. por favor vuelve a completar el formulario.
                </div>
                <div class="pos">
                    <div class="rel">
                     <form id="contact-form" data-abide='ajax' data-toggle="validator" role="form">
                            <!--<div class="cotizacion"></div>-->
                                <fieldset id="step-1" class="step animated fadeInUp active" data-step="1">
                                    <h1>Cotizador online</h1>
                                    <p class="lead">Complete con sus datos y seleccione el tipo de seguro</p>
                                    <div class="form-group">
                                        <label>¿Tipo de seguro?</label>
                                        <select class="form-control required" name="seguro" id="seguro">
                                            <option value="0">Seleccionar</option>
                                            <option value="alquiler">Alquileres</option>
                                            <option value="obra">Obras</option>
                                            <option value="servicios">Servicios</option>
                                            <option value="sociedades">Sociedades</option>
                                            <option value="aduanera">Aduanera</option>
                                            <option value="ambiental">Ambiental (SAO)</option> <!--Agregada opción ambiental-->
                                            <option value="otros">Otros</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Elije un tipo de seguro
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Nombre y apellido</label>
                                        <input type="text" class="form-control required" id="nombre" name="nombre"
                                               aria-describedby="emailHelp" required>
                                        <div class="invalid-feedback">
                                            Ingresa tu nombre
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <input type="tel" class="form-control required" id="telefono" name="telefono"
                                               aria-describedby="emailHelp" required>
                                        <div class="invalid-feedback">
                                            Ingresa tu teléfono
                                        </div>
                                    </div>
                                    <div class="form-group" esto="Cosjj">
                                        <label>Email</label>
                                        <input type="email" class="form-control required" id="email"  name="email">
                                        <div class="invalid-feedback">
                                            Ingresa tu email
                                        </div>
                                    </div>

                                    <div class="form-group boton">
                                        <button class="siguiente qbutton mt-1 ">CONTINUAR <i
                                                    class="fas fa-arrow-right"></i>
                                        </button>
                                    </div>
                                </fieldset>
                                <fieldset id="step-2" class="step animated" data-step="2">
                                    <div id="alquileres" class="tipo d-none">
                                        <h1>Tipo de seguro.</h1>
                                        <p class="lead">Seleccione el seguro que necesita cotizar.</p>
                                        <div class="animate alquileres" id="collapse">
                                            <div class="form-group">
                                                <label>¿Tipo de alquiler?</label>
                                                <select class="form-control" name="tipo_alquiler" id="alquiler">
                                                    <option value="0">Seleccionar</option>
                                                    <option value="comercial">Comercial</option>
                                                    <option value="vivienda">Vivienda</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Ingresos mensuales del grupo familiar</label>
                                            <input class="form-control money" id="ingresos" name="ingresos">
                                            <div class="invalid-feedback">
                                                Completa el campo
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Cuit del inquilino</label>
                                            <input class="form-control cuit" id="cuitInquilino"  name="cuit_Inquilino">
                                            <div class="invalid-feedback">
                                                Ingresa el cuit correspondiente
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label>¿Cuánto vas a pagar por mes de alquiler?</label>
                                            <input class="form-control money" id="importe"
                                                   name="importe">
                                            <div class="invalid-feedback">
                                                Ingresa el importe
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>¿Querés sumar las expensas a la cobertura</label>
                                            <select class="form-control" name="incluye_Expensas" id="incExpensas">
                                                <option value="0">Seleccionar</option>
                                                <option value="si">Si</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>
                                        <div id="idExp" class="form-group d-none">
                                            <label>Expensas</label>
                                            <input class="form-control money" id="expensas"
                                                   name="expensas">
                                            <div class="invalid-feedback">
                                                Ingresa el valor de expensas
                                            </div>
                                        </div>
                                        <div class="form-group boton">
                                            <button  type="submit" class="qbutton mt-1 " id="submit">CONTINUAR <i class="fas fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div id="obras" class="tipo d-none">
                                        <h1>Tipo de seguro.</h1>
                                        <p class="lead">Seleccione el seguro que necesita cotizar.</p>
                                        <div class="form-group">
                                            <label>Cuit empresa</label>
                                            <input class="form-control cuit" id="cuitEmpresa"  name="cuit_Empresa">
                                            <div class="invalid-feedback">
                                                Ingresa el cuit correspondiente
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Razón Social</label>
                                            <input type="text" class="form-control" id="razonSocial" name="razon_Social"
                                                   aria-describedby="emailHelp">
                                            <div class="invalid-feedback">
                                                Ingresa tu razón social
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Monto del contrato</label>
                                            <input class="form-control money" id="monto" placeholder="$"
                                                   name="monto" value="">
                                            <div class="invalid-feedback">
                                                Ingresa el monto del contrato
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Suma Asegurada</label>
                                            <input class="form-control money" id="suma"
                                                   name="suma">
                                            <div class="invalid-feedback">
                                                Ingresa la suma asegurada
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Patrimonio neto de la empresa</label>
                                            <input class="form-control money" id="patrimonio"
                                                   name="patrimonio">
                                            <div class="invalid-feedback">
                                                Ingresa el patrimonio neto
                                            </div>
                                        </div>
                                        <div class="form-group boton">
                                            <button class="qbutton mt-1 " id="submit">CONTINUAR <i class="fas fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div id="sociedades" class="tipo d-none">
                                        <h1>Tipo de seguro.</h1>
                                        <p class="lead">Seleccione el seguro que necesita cotizar.</p>
                                        <div class="form-group">
                                            <label>Capital a Asegurar</label>
                                            <input class="form-control money" id="capital" name="capital"
                                                   placeholder="$">
                                            <div class="invalid-feedback">
                                                Completa el campo
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Cantidad de socios</label>
                                            <input type="number" class="form-control" id="socios" name="numero_socios">
                                            <div class="invalid-feedback">
                                                Ingresa la cantidad de socios
                                            </div>
                                        </div>
                                        <div class="form-group boton">
                                            <button class="qbutton mt-1 " id="submit">CONTINUAR <i class="fas fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div id="aduana" class="d-none">
                                        <h1>Tipo de seguro.</h1>
                                        <p class="lead">Seleccione el seguro que necesita cotizar.</p>
                                        <div class="form-group">
                                            <label>Tipo de Caución Aduanera</label>
                                            <input type="text" class="form-control" id="tipoCaucionAduana" name="tipo_Caucion"
                                                   aria-describedby="emailHelp">
                                            <div class="invalid-feedback">
                                                Ingresa tu Tipo de Caución
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Cuit empresa</label>
                                            <input class="form-control cuit" id="cuitAduana" name="cuit_empresas">
                                            <div class="invalid-feedback">
                                                Ingresa el cuit correspondiente
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Razón Social</label>
                                            <input type="text" class="form-control" id="razonAduana" name="razon_Social_Empresa"
                                                   aria-describedby="emailHelp">
                                            <div class="invalid-feedback">
                                                Ingresa tu razón social
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Suma Asegurada</label>
                                            <input class="form-control money" id="sumaAduana"
                                                   name="suma_Asegurada">
                                            <div class="invalid-feedback">
                                                Ingresa la suma asegurada
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Patrimonio neto de la empresa</label>
                                            <input class="form-control money" id="patrimoniAduana"
                                                   name="patrimonio_Empresa">
                                            <div class="invalid-feedback">
                                                Ingresa el patrimonio neto
                                            </div>
                                        </div>
                                        <div class="form-group boton">
                                            <button class="qbutton mt-1 " id="submit">CONTINUAR <i class="fas fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>


                                    <!-- Agregado ambiental -->


                                    <div id="ambiental" class="d-none">
                                        <h1>Tipo de seguro.</h1>
                                        <p class="lead">Seleccione el seguro que necesita cotizar.</p>
                                        <div class="form-group">
                                            <label>Cuit empresa</label>
                                            <input class="form-control cuit" id="cuitAmbiental"  name="cuit_empresas">
                                            <div class="invalid-feedback">
                                                Ingresa el cuit correspondiente
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Razón Social</label>
                                            <input type="text" class="form-control" id="razonAmbiental" name="razon_Social_Empresa"
                                                   aria-describedby="emailHelp">
                                            <div class="invalid-feedback">
                                                Ingresa tu razón social
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Suma Asegurada</label>
                                            <input class="form-control money" id="sumaAmbiental"
                                                   name="suma_Asegurada">
                                            <div class="invalid-feedback">
                                                Ingresa la suma asegurada
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Otra información que quiera agregar</label>
                                            <textarea class="form-control" id="informacionAmbiental"
                                                   name="informacion_Empresa"></textarea>
                                            <div class="invalid-feedback">
                                                Ingresa la información
                                            </div>
                                        </div>
                                        <div class="form-group boton">
                                            <button class="qbutton mt-1 " id="submit">CONTINUAR <i class="fas fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <!-- fin Agregado ambiental -->


                                    <div id="otros" class="d-none">
                                        <h1>Tipo de seguro.</h1>
                                        <p class="lead">Seleccione el seguro que necesita cotizar.</p>
                                        <div class="form-group">
                                            <label>Tipo de Caución</label>
                                            <input type="text" class="form-control" id="tipoCaucion" name="tipo_Caucion"
                                                   aria-describedby="emailHelp">
                                            <div class="invalid-feedback">
                                                Ingresa tu Tipo de Caución
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Cuit empresa</label>
                                            <input class="form-control cuit" id="cuitOtros"  name="cuit_empresas">
                                            <div class="invalid-feedback">
                                                Ingresa el cuit correspondiente
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Razón Social</label>
                                            <input type="text" class="form-control" id="razonOtros" name="razon_Social_Empresa"
                                                   aria-describedby="emailHelp">
                                            <div class="invalid-feedback">
                                                Ingresa tu razón social
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Monto del contrato</label>
                                            <input class="form-control money" id="montoOtros"
                                                   name="monto_Contrato">
                                            <div class="invalid-feedback">
                                                Ingresa el monto del contrato
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Suma Asegurada</label>
                                            <input class="form-control money" id="sumaOtros"
                                                   name="suma_Asegurada">
                                            <div class="invalid-feedback">
                                                Ingresa la suma asegurada
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Otra información que quiera agregar</label>
                                            <textarea class="form-control" id="informacionOtros"
                                                   name="informacion_Empresa"></textarea>
                                            <div class="invalid-feedback">
                                                Ingresa la información
                                            </div>
                                        </div>
                                        <div class="form-group boton">
                                            <button class="qbutton mt-1 " id="submit">CONTINUAR <i class="fas fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="step-3" class="step animated" data-step="3">
                                    <div id="cotizaciones">
                                        <h1>La cotización para tu seguro es de <span id="total"></span> <span id="socio-asegurado" class="d-none"> por socio asegurado</span> </h1>
                                        <h3 id="prima" class="d-none">Prima Trimestral (no incluye Impuestos)</h3>
                                        <h3>
                                            Te enviamos un correo con la información sobre este seguro. Si lo desea puede llamarnos al 011 6009 5100.
                                        </h3>
                                        <hr>
                                        <p id="alquiler-incremento" class="d-none">La presente cotización es de referencia, se considera un incremento semestral del 15% para el alquiler y las expensas. </p>
                                        <p>La información indicada en esta página está sujeta a verificación comercial y crediticia de Foms Cia. Argentina de Seguros SA y a la presentación de documentación respaldatoria.</p>
                                    </div>
                                    <div id="solicitud">
                                        <h1>Recibimos su solicitud, en breve un representante comercial se comunicará con UD.</h1>
                                        <h3>
                                            Cualquier duda nuestro teléfono de contacto es 011 6009 5100.
                                        </h3>
                                        <hr>
                                    </div>
                                    <a class="qbutton mt-1 d-none" id="reset" href="<?php echo home_url()  ?>/caucion-cotizacion">Volver a Cotizar</a>
                                </fieldset>
                                <input class="form-control" type="hidden" name="cotizacion" id="cotizacion" value=""/>
                                <input type="hidden" name="submitted" id="submitted" value="true"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row icons">
       <div class="col-12">
           <?php the_content(); ?>
       </div>
    </div>
</div>
<script type="text/javascript">

</script>
<?php get_footer(); ?>


