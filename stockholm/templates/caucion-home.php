<?php
/**
 * Template Name: Caución Home
 *
 */
get_header(); ?>
<div class="container bg-primary h-100" id="contenido" >
    <div class="row justify-content-center align-content-center align-items-center h-100" >
        <div class="col-lg-8 my-4" >
            <h1 ><?php the_title(); ?></h1 >
            <?php the_content() ?>
            <a href="<?php echo home_url() ?>/caucion-cotizacion" class="qbutton"> COTIZAR ONLINE</a>
        </div >
    </div >
</div >
<div class="bg-white">
    <div class="container" id="icon-container">
        <div class="vc_row wpb_row section vc_row-fluid bloques vc_custom_1529094637886 grid_section"
             style=" padding-top:0px; padding-bottom:0px; text-align:left;" >
            <div class=" section_inner clearfix" >
                <div class="section_inner_margin clearfix" >
                    <div class="wpb_column vc_column_container vc_col-sm-4" >
                        <div class="vc_column-inner " >
                            <div class="wpb_wrapper" >
                                <div class="wpb_single_image wpb_content_element vc_align_center" >
                                    <div class="wpb_wrapper" >

                                        <div class="vc_single_image-wrapper   vc_box_border_grey" ><img width="106"
                                                                                                        height="104"
                                                                                                        src="http://foms.local/wp-content/uploads/2018/06/icn_emision.jpg"
                                                                                                        class="vc_single_image-img attachment-full"
                                                                                                        alt="" ></div >
                                    </div >
                                </div >

                                <div class="wpb_text_column wpb_content_element  vc_custom_1529094671110" >
                                    <div class="wpb_wrapper" >
                                        <p style="text-align: center;" >Emisión de Pólizas<br >
                                            en el día.</p >

                                    </div >
                                </div >
                            </div >
                        </div >
                    </div >
                    <div class="wpb_column vc_column_container vc_col-sm-4" >
                        <div class="vc_column-inner " >
                            <div class="wpb_wrapper" >
                                <div class="wpb_single_image wpb_content_element vc_align_center" >
                                    <div class="wpb_wrapper" >

                                        <div class="vc_single_image-wrapper   vc_box_border_grey" ><img width="106"
                                                                                                        height="104"
                                                                                                        src="http://foms.local/wp-content/uploads/2018/06/icn_chat.jpg"
                                                                                                        class="vc_single_image-img attachment-full"
                                                                                                        alt="" ></div >
                                    </div >
                                </div >

                                <div class="wpb_text_column wpb_content_element  vc_custom_1534514721361" >
                                    <div class="wpb_wrapper" >
                                        <p style="text-align: center;" >Chat de atención<br >
                                            Online</p >

                                    </div >
                                </div >
                            </div >
                        </div >
                    </div >
                    <div class="wpb_column vc_column_container vc_col-sm-4" >
                        <div class="vc_column-inner " >
                            <div class="wpb_wrapper" >
                                <div class="wpb_single_image wpb_content_element vc_align_center" >
                                    <div class="wpb_wrapper" >

                                        <div class="vc_single_image-wrapper   vc_box_border_grey" ><img width="106"
                                                                                                        height="104"
                                                                                                        src="http://foms.local/wp-content/uploads/2018/06/icn_tiempo.jpg"
                                                                                                        class="vc_single_image-img attachment-full"
                                                                                                        alt="" ></div >
                                    </div >
                                </div >

                                <div class="wpb_text_column wpb_content_element  vc_custom_1529094679543" >
                                    <div class="wpb_wrapper" >
                                        <p style="text-align: center;" >Lo hacemos simple,<br >
                                            optimizamos tus tiempos.</p >

                                    </div >
                                </div >
                            </div >
                        </div >
                    </div >
                </div >
            </div >
        </div >
    </div >
</div >
<?php get_footer(); ?>
