jQuery.fn.extend({
    animateCss: function (animationName, callback) {
        var animationEnd = (function (el) {
            var animations = {
                animation: 'animationend',
                OAnimation: 'oAnimationEnd',
                MozAnimation: 'mozAnimationEnd',
                WebkitAnimation: 'webkitAnimationEnd',
            };

            for (var t in animations) {
                if (el.style[t] !== undefined) {
                    return animations[t];
                }
            }
        })(document.createElement('div'));

        this.addClass('animated ' + animationName).one(animationEnd, function () {
            jQuery(this).removeClass('animated ' + animationName);

            if (typeof callback === 'function') callback();
        });
        return this;
    },
});

function submitForm_contact() {

    var info = jQuery('.form-control');
    var newInfo = {};

    if (seguro != '' && nombre != '' && telefono != '' && email != '') {
        jQuery('#contact-form #submit').click(function (event) {
            validate();
            cotizacion();
            var fun;
            var home_url = jQuery('#url').html();
            var url = window.location.href;
            fun = validate();
            //console.log(fun);
            //alert(jQuery('#subsidio').val());

            if (fun) {

                jQuery(this).submit();

                info.each(function (index, el) {
                    if (el.value != '' && el.value != 0) {
                        if (el.name == 'posee_Subsidio') {
                            if (jQuery('#subsidio').is(':checked')) {
                                newInfo[el.name] = 'dale';
                            } else {
                                newInfo[el.name] = el.value;
                            }
                        } else {
                            newInfo[el.name] = el.value;
                        }
                    }
                });
                newInfo = JSON.stringify(newInfo);

                jQuery('#reset').removeClass('d-none');
                jQuery('#contact-form #submit').text('COTIZANDO...');
                jQuery('#contact-form #submit').attr('disabled', 'disabled');

                if (url.includes('garantiaba')) {
                    ajax(newInfo, home_url + '/caucion-envio-garantiaba');
                } else {
                    ajax(newInfo, home_url + '/caucion-envio/');
                }


            } else {
                event.preventDefault();
            }

        });


    } else {
        validate();
    }
}

function ajax(newInfo, url) {
    //jQuery('#alerta').addClass('d-none');

    //console.log('newinfo  ',newInfo);

    jQuery.ajax({
        type: "POST",
        url: url,
        data: {datos: newInfo},
        success: function (data) {
            if (data == 'ok') {
                cotizacion();
                nexter();
            } else {
                jQuery('.mail-error').removeClass('d-none');
                nexter();
            }
        }
    });
}

function validate() {
    var valu;
    var active = jQuery('fieldset.active');
    var step = active.data('step');
    var fieldset = jQuery('#step2');

    if (jQuery('fieldset.active').data(step) == 1 && jQuery('#seguro').val() != "alquiler" && !jQuery('.tipo').hasClass('d-none')) {

        if (jQuery('#seguro').val() == 0 || jQuery(this).val() == '') {
            jQuery('#seguro').addClass('is-invalid');
            jQuery('#seguro').next().addClass('show');
        } else {
            jQuery(this).removeClass('is-invalid').next().removeClass('show');
        }
    } else {
        jQuery('fieldset.active .required').each(function () {

            if (jQuery(this).val() == 0 || jQuery(this).val() == '' || jQuery(this).val() == '$') {
                jQuery(this).addClass('is-invalid');
                jQuery(this).next().addClass('show');
                valu = false;
            } else {
                jQuery(this).removeClass('is-invalid').next().removeClass('show');
                valu = true;
            }
        });
        jQuery('fieldset.active .required').each(function () {
            if (jQuery(this).hasClass('is-invalid')) {
                valu = false;
            }
        });
    }

    return valu;
}

function nexter() {
    validate();
    var active = jQuery('fieldset.active');
    var seg = jQuery('#contact-form #seguro');
    var step = jQuery('.counter-step.active');
    if (seg.val() == 'alquiler') {
        jQuery('#alquileres').removeClass('d-none');
        jQuery('#alquiler-incremento').removeClass('d-none');
        jQuery('#alquiler').addClass('required');
        jQuery('#ingresos').addClass('required');
        jQuery('#cuitInquilino').addClass('required');
        jQuery('#importe').addClass('required');

    } else if (seg.val() == 'obra' || seg.val() == 'servicios') {     
        jQuery('#obras').removeClass('d-none');
        jQuery('#cuitEmpresa').addClass('required');
        jQuery('#razonSocial').addClass('required');
        jQuery('#monto').addClass('required');
        jQuery('#suma').addClass('required');
        jQuery('#patrimonio').addClass('required');
    } else if (seg.val() == 'sociedades') {
        jQuery('#sociedades').removeClass('d-none');
        jQuery('#capital').addClass('required');
        jQuery('#socios').addClass('required');

    } else if (seg.val() == 'aduanera') {
        jQuery('#aduana').removeClass('d-none');
        jQuery('#tipoCaucionAduana').addClass('required');
        jQuery('#cuitAduana').addClass('required');
        jQuery('#razonAduana').addClass('required');
        jQuery('#sumaAduana').addClass('required');
        jQuery('#patrimonioAduana').addClass('required');
    } else if (seg.val() == 'otros') {
        jQuery('#otros').removeClass('d-none');
        jQuery('#tipoCaucion').addClass('required');
        jQuery('#cuitOtros').addClass('required');
        jQuery('#razonOtros').addClass('required');
        jQuery('#montoOtros').addClass('required');
        jQuery('#sumaOtros').addClass('required');
        jQuery('#patrimonioOtros').addClass('required');
    } else if (seg.val() == 'ambiental') { //agregada opción ambiental
        jQuery('#ambiental').removeClass('d-none');
        jQuery('#cuitAmbiental').addClass('required');
        jQuery('#razonAmbiental').addClass('required');
        jQuery('#sumaAmbiental').addClass('required');
    }


    if (jQuery(".invalid-feedback.show", active).length === 0 && !jQuery('#step-1').hasClass('alquiler')) {
        step.next().addClass('active');
        active.addClass('fadeOutUp').removeClass('active fadeInUp fadeInDown');
        active.next().addClass('fadeInUp active');
    }

    if (jQuery('#step-3').hasClass('active') || jQuery('#step-2').hasClass('active')) {
        jQuery('html').animate({
            scrollTop: 0
        }, 'slow');
    }
}

function cotizacion() {
    var seguro = jQuery('#seguro').val();
    var url = window.location.href;
    if (seguro == 'alquiler') {
        var alquiler = jQuery('#alquiler').val();
        var importe = jQuery('#importe').inputmask('unmaskedvalue');
        var ingresos = jQuery('#ingresos').inputmask('unmaskedvalue');
        var expensas = jQuery('#expensas').inputmask('unmaskedvalue');
        var meses = 24;

        ingresos = parseFloat(ingresos);
        expensas = parseFloat(expensas);
        importe = parseFloat(importe);

        if (alquiler == 'comercial') {
            meses = 36;
        }
        var incremento = meses / 6;
        var impAcum = 0;
        var expAcum = 0;
        for (let i = 0; i < incremento; i++) {
            imp = importe * 6;
            //console.log(imp);
            var element = importe * 0.15;
            importe = importe + element;
            var exp = expensas * 6;
            var elem = expensas * 0.15;
            expensas = expensas + elem;
            impAcum = impAcum + imp;
            expAcum = expAcum + exp;
            /*console.log('importe  '+ importe);
            console.log('Expensas ' + expensas);
            console.log('Importes   ' + imp);
            console.log('Expensas    ' + exp);*/

        }
        if (isNaN(expAcum) || jQuery('#incExpensas').val() == 'no') {
            expAcum = 0;
        }
        //console.log('importe Acumulador  '+ impAcum);
        //console.log('Expensas Acumulador ' + expAcum);
        var impTotal = parseFloat(impAcum) + parseFloat(expAcum);

        var sueldo_anual = ingresos * meses;
        var cuenta = (100 * impTotal) / sueldo_anual;
        var porcentaje = 4;


        if (cuenta > 0 && cuenta <= 20.99) {
            porcentaje = 4.5;
        }
        if (cuenta >= 21 && cuenta <= 40) {
            porcentaje = 5.5;
        }
        if (cuenta > 40) {
            porcentaje = 0;
        }

        var subsidio = 0;
        if (url.includes('garantiaba')) {
            porcentaje = 4;
            if (jQuery('#subsidio').prop("checked")) {
                subsidio = 0.3;
            }
        }


        //console.log('porcentaje',porcentaje);
        //console.log('impTotal',impTotal);


        var total = impTotal * porcentaje / 100;
        if (url.includes('garantiaba')) {
            var desc = total * subsidio;
            total = total - desc;
        }
        //console.log(total);


        jQuery('.cotizacion').html(currency(total));
        jQuery('#cotizacion').val(currency(total));
        jQuery('#total').html(currency(total));

        if (total < 300 && !url.includes('garantiaba')) {
            jQuery('#solicitud').removeClass('d-none');
            jQuery('#cotizaciones').addClass('d-none');
        } else {
            jQuery('#solicitud').addClass('d-none');
            jQuery('#cotizaciones').removeClass('d-none');
        }
    } else if (seguro == 'obra' || seguro == 'servicios') { 
        var monto = jQuery('#monto').inputmask('unmaskedvalue');
        var suma = jQuery('#suma').inputmask('unmaskedvalue');
        var patrimonio = jQuery('#patrimonio').inputmask('unmaskedvalue');
        var tasa;
        var porcentaje = (suma * 100) / patrimonio;

        if (porcentaje < 30) {
            porcentaje = 30;
        }

        if (porcentaje == 30) {
            tasa = 0.005;
        } else if (porcentaje > 30 && porcentaje <= 70) {
            tasa = 0.0075;
        } else if (porcentaje > 70 && porcentaje <= 100) {
            tasa = 0.01;
        } else if (porcentaje > 100 && porcentaje <= 150) {
            tasa = 0.0125;
        } else if (porcentaje > 150 && porcentaje <= 165) {
            tasa = 0.015;
        } else if (porcentaje > 165 && porcentaje <= 200) {
            tasa = 0.02;
        } else if (porcentaje > 200 && porcentaje <= 250) {
            tasa = 0.025;
        } else if (porcentaje > 250 && porcentaje <= 400) {
            tasa = 0.03;
        } else if (porcentaje > 400 && porcentaje <= 600) {
            tasa = 0.035;
        } else if (porcentaje > 600 && porcentaje <= 1000) {
            tasa = 0.04;
        } else {
            tasa = 0.04;
        }
        suma = suma.replace(',', '.');

        var total = (suma * tasa) / 4;
        jQuery('.cotizacion').html(currency(total));
        jQuery('#cotizacion').val(currency(total));
        jQuery('#total').html(currency(total));

        if (total < 300) {
            jQuery('#solicitud').removeClass('d-none');
            jQuery('#cotizaciones').addClass('d-none');
        } else {


            jQuery('#monto').val(monto);

            if (seguro == 'obra' || seguro == 'servicios') { 
                jQuery('#prima').removeClass('d-none');
            }
            jQuery('#solicitud').addClass('d-none');
            jQuery('#cotizaciones').removeClass('d-none');
        }
    } else if (seguro == 'sociedades') {
        var capital = jQuery('#capital').inputmask('unmaskedvalue');
        var socios = jQuery('#socios').val();
        var montoFijo;
        capital = capital.replace(',', '.');
        jQuery('#socio-asegurado').removeClass('d-none');

        if (capital > 50000) {
            capital = 50000;
        }

        if (capital <= 10000) {
            montoFijo = 400.00;
        } else if (capital > 10000 && capital <= 20000) {
            montoFijo = 720.00;
        } else if (capital > 20000 && capital <= 30000) {
            montoFijo = 1050.00
        } else if (capital > 30000 && capital <= 40000) {
            montoFijo = 1350.00;
        } else if (capital > 40000 && capital <= 50000) {
            montoFijo = 1600.00;
        }
        jQuery('.cotizacion').html(currency(montoFijo));
        jQuery('#cotizacion').val(currency(montoFijo));
        jQuery('#total').html(currency(montoFijo));
        jQuery('#solicitud').addClass('d-none');
        if (seguro == 'obra' || seguro == 'servicios') { 
            jQuery('#prima').removeClass('d-none');
        }
        if (montoFijo < 300) {
            jQuery('#solicitud').removeClass('d-none');
            jQuery('#cotizaciones').addClass('d-none');
        } else {
            jQuery('#solicitud').addClass('d-none');
            jQuery('#cotizaciones').removeClass('d-none');
        }
    } else if (seguro == 'aduanera' || seguro == 'otros' || seguro == 'ambiental') { //agregado ambiental
        jQuery('#cotizaciones').addClass('d-none');
    }
    jQuery('#alerta').addClass('d-none');
}

function currency(monto) {
    const formatter = new Intl.NumberFormat('es-IT', {
        style: 'currency',
        currency: 'ARS',
        currencyDisplay: 'symbol',
        minimumFractionDigits: 2
    });
    var newCurr = formatter.format(monto).replace('ARS', '');
    return '$' + newCurr;
}

jQuery('fieldset#reset').on('click', function (event) {
    event.preventDefault();
    location.reload();
});


jQuery(document).ready(function () {


    if (window.location.search != "") {
        var newInfo = {};
        var urlParams = new URLSearchParams(window.location.search);
        var tipoAlq = urlParams.get('tipoAlquiler');
        var email = urlParams.get('email');
        var ingreso = urlParams.get('ingresos');
        var precioAlq = urlParams.get('precioAlquiler');
        var err = urlParams.get('err');
        newInfo.tipo_alquiler = tipoAlq;
        newInfo.email = email;
        newInfo.seguro = 'alquiler';
        newInfo.ingresos = ingreso;
        newInfo.importe = importe;
        //alert(precAlq);
        var home_url = jQuery('#url').html();

        if (err == 1) {
            jQuery('#alerta').removeClass('d-none');
        }

        if (tipoAlq == '' || email == '' || ingreso == '' || importe == '') {
            window.location.href = home_url + '/caucion-cotizacion?err=1';
        } else {
            //var total = importe * 24;

            /*for (let i = 0; i < 4; i++) {
                var element = total * 0.125;
                total=total+element;        
            }


            jQuery('#total').html(currency(total));

            if (total < 300) {
                jQuery('#solicitud').removeClass('d-none');
                jQuery('#cotizaciones').addClass('d-none');
            } else {
                jQuery('#solicitud').addClass('d-none');
                jQuery('#cotizaciones').removeClass('d-none');
            }*/

            var ingresos = parseFloat(ingreso);
            var importe = parseFloat(precioAlq);

            var meses = 24;
            var incremento = meses / 6;
            var impAcum = 0;
            var expAcum = 0;
            for (let i = 0; i < incremento; i++) {
                imp = importe * 6;
                //console.log(imp);
                var element = importe * 0.125;
                importe = importe + element;
                impAcum = impAcum + imp;

            }
            if (isNaN(expAcum) || jQuery('#incExpensas').val() == 'no') {
                expAcum = 0;
            }
            //console.log('importe Acumulador  '+ impAcum);
            //console.log('Expensas Acumulador ' + expAcum);
            var impTotal = parseFloat(impAcum) + parseFloat(expAcum);

            var sueldo_anual = ingresos * meses;
            var cuenta = (100 * impTotal) / sueldo_anual;
            var porcentaje = 4;

            if (cuenta > 0 && cuenta <= 20.99) {
                porcentaje = 4.5;
            }
            if (cuenta >= 21 && cuenta <= 40) {
                porcentaje = 5.5;
            }
            if (cuenta > 40) {
                porcentaje = 0;
            }

            var total = impTotal * porcentaje / 100;

            jQuery('#total').html(currency(total));

            if (total < 300) {
                jQuery('#solicitud').removeClass('d-none');
                jQuery('#cotizaciones').addClass('d-none');
            } else {
                jQuery('#solicitud').addClass('d-none');
                jQuery('#cotizaciones').removeClass('d-none');
            }

            jQuery('.lds-roller').addClass('d-none');
            jQuery('#step-1').addClass('fadeInUp active');

            newInfo.ingresos = currency(ingreso);
            newInfo.importe = currency(precioAlq);
            newInfo.cotizacion = currency(total);

            newInfo = JSON.stringify(newInfo);

            console.log(home_url);

            ajax(newInfo, home_url + '/caucion-envio-alquileres');


        }


    } else {
        var home_url = window.location.href;
        if (home_url.includes('alquiler')) {
            window.location.href = home_url + '/caucion-cotizacion?err=1';
        }
    }

    jQuery('.money').inputmask("numeric", {
        radixPoint: ",",
        groupSeparator: ".",
        greedy: false,
        digits: 2,
        autoGroup: true,
        prefix: '$', //No Space, this will truncate the first character
        rightAlign: false,
        oncleared: function () {
            self.Value('');
        }
    });
    jQuery('.cuit').inputmask({mask: "99-99999999-9"});

    jQuery('#contact-form').on('submit', function (e) {
        e.preventDefault();

        submitForm_contact();


    });
    jQuery("#seguro").keyup(function () {
        if (jQuery('#seguro').val() == "alquiler" || jQuery('#seguro').val() == "obra" || jQuery('#seguro').val() == "servicios") { 
            jQuery('#collapse.otros').addClass('d-none');
            jQuery('#collapse.alquileres').removeClass('d-none').addClass('fadeInUp');
        } else {
            jQuery('#collapse.otros').removeClass('d-none').addClass('fadeInUp');
            jQuery('#collapse.alquileres .form-control').removeClass('required');
            validate();
        }
    });

    jQuery('#incExpensas').change(function () {
        if (jQuery('#incExpensas').val() == 'si') {
            jQuery('#idExp').removeClass('d-none');
        } else {
            jQuery('#idExp').addClass('d-none');
        }
    });

    jQuery('.required').keyup(function () {
        var empty = jQuery(this).val() == "";
        jQuery(this).next().toggleClass("", !empty).toggleClass("show", empty)
    });

    var valid = false;

    jQuery('.form-control').not('#seguro').on('keyup', function () {
        validate();
        cotizacion();
    });
    jQuery('#incExpensas').change(function () {
        validate();
        cotizacion();
        //console.log('change');

    });

    jQuery('#subsidio').change(function () {
        validate();
        cotizacion();
    });

    jQuery(".siguiente").click(function () {
        nexter();
    });
    jQuery(".anterior").click(function () {

        prever();
    });
});