<?php
/**
 * Template Name: Caución Form 
 *
 */
 ?>
<?php get_header(); ?>
<div id="contenido">
    <div id="url" class="d-none"><?php echo home_url(); ?></div>
    <div class="fondo d-xs-none"></div>
    <div class="container">
        <div class="row bd-title justify-content-center">
            <div class="col-lg-6">
                <div class="cinta">
                    <h1>SEGURO DE CAUCIÓN</h1>
                    <h2>COBERTURA EN TODO EL PAÍS</h2>
                </div>
            </div>
            <div class="col-lg-5 bg-light pad">
                <div class="pos">
                    <div class="rel">
                     <form id="contact-form" data-abide='ajax' data-toggle="validator" role="form">
                     <div class="lds-roller justify-content-center align-self-center"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        <fieldset id="step-1" class="step animated alquiler" data-step="1">
                            <div id="cotizaciones">
                                <h1>La cotización para tu seguro es de <span id="total"></span> <span id="socio-asegurado" class="d-none"> por socio asegurado</span> </h1>
                                <h3 id="prima" class="d-none">Prima Trimestral (no incluye Impuestos)</h3>
                                <h3>
                                    Te enviamos un correo con la información sobre este seguro. Si lo desea puede llamarnos al 011 6009 5100.
                                </h3>
                                <hr>
                                <p>La presente cotización está sujeta a verificación comercial y crediticia de Foms Cia. Argentina de Seguros y a la presentación de la documentación respaldatoria. </p>
                            </div>
                            <div id="solicitud">
                                <h1>Recibimos su solicitud, en breve un representante comercial se comunicará con UD.</h1>
                                <h3>
                                    Cualquier duda nuestro teléfono de contacto es 011 6009 5100.
                                </h3>
                                <hr>
                            </div>
                            <a class="qbutton mt-1 d-none" id="reset" href="<?php echo home_url()  ?>/caucion-cotizacion">Volver a Cotizar</a>
                        </fieldset>
                        <input class="form-control" type="hidden" name="cotizacion" id="cotizacion" value=""/>
                        <input type="hidden" name="submitted" id="submitted" value="true"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row icons">
       <div class="col-12">
           <?php the_content(); ?>
       </div>
    </div>
</div>
<script type="text/javascript">

</script>
<?php get_footer(); ?>


