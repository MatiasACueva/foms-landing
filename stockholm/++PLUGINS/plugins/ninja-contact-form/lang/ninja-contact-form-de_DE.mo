��    ?                         $   &  #   K      o     �     �     �  &   �            
   +     6     >     E     R  5   X     �     �     �     �  
   �     �     �     �     �     �  	          	        "     2     C     Q  ,   Y     �     �     �     �  5   �     �  	   �     �     �  (        4     =     B     Q     g  /   x     �  	   �  ;   �     �               !  #   .  	   R  
   \     g     �  �  �       +   �  '   �      �          0     N      h      �     �     �     �     �     �  
   �  E   �     :     @     R     l     {     �     �     �     �  "   �  	   �     �     �     �     �       	     A   $     f     k     �     �  =   �     �  
   �  
   �     �  '        /     =     D     Y     x  6   �     �     �  <   �     )     7     H     U  +   f  	   �     �     �     �    Contact Form Submission * %% can have at most %% characters. * %% must be a valid email address. * %% must be a valid ip address. * %% must be greater than %%. * %% must be less than %% * %% must be numeric. * %% must have at least %% characters. * %% must match %%. * Please enter %% * Wrong %% Address Answer Browser info Close Comma Separated list of IDs of posts not listed above Company Custom Post Types Default WP pages Display rule: Don't show Email Email subject Everyone Front Hide on checked pages Home/Blog IP Languages Logged-in users Logged-out users Looks Awesome Message Message not sent. An unknown error occurred. Name Ninja Kick: Contact Form Page Phone Push/Sliding Contact Form on every page of your site. Send Send more Sending Sent from page Sent via Ninja Kick: Contact Form plugin Settings Show Show Form for: Show on checked pages Show on mobiles: Something went wrong while sending your message Subscription Try again You do not have sufficient permissions to access this page. Your address Your company Your e-mail Your message Your message was successfully sent! Your name Your phone contactform.looks-awesome.com http://looks-awesome.com Project-Id-Version: Ninja Kick: Contact Form v3.1.5
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-02 15:32+0000
PO-Revision-Date: 2018-03-02 15:59+0000
Last-Translator: Der Kakadu <natascha@kakadukid.de>
Language-Team: German
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1
X-Generator: Loco - https://localise.biz/
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-SearchPath-0: .
X-Textdomain-Support: yes
Language: de-DE Sendung per Kontaktformular * %% kann auf die meisten haben %% Zeichen. * %% muss eine gültige E-Mail-Adresse. * %% must be a valid ip address. * %% muss größer sein als %%. * %% muss kleiner sein als %% * %% muss numerisch sein. * %% muss mindestens %% Zeichen. * %% müssen übereinstimmen %%. * Bitte eingeben: %% * Falsche %% Adresse Antwort Browser-Info Schließen Kommaseparierte Liste mit IDs von oben nicht aufgelisteten Beiträgen Firma Custom Post Types Standard Wordpress-Seiten Anzeige-Regeln Nicht zeigen E-Mail E-Mail-Betreff Alle Start Verstecken auf angekreuzten Seiten Home/Blog IP Sprachen Eingeloggte Benutzer Ausgeloggte Benutzer Looks Awesome Nachricht Nachricht nicht gesendet. Ein unbekannter Fehler ist aufgetreten. Name Ninja Kick: Kontaktformular Seite Telefon Push/Sliding Kontaktformular auf allen Seiten deiner Website. Senden Sende mehr Versendung Gesendet von der Seite Gesendet vom Ninja Kick Kontaktformular Einstellungen Zeigen Zeige Formular für: Zeigen auf angekreuzten Seiten Auf Mobiltelefon zeigen: Etwas ist schief gelaufen beim Senden deiner Nachricht Subskription Bitte erneut versuchen Du hast nicht die nötigen Rechte für den Zugang zur Seite. Deine Adresse Dein Unternehmen Deine E-Mail Deine Mitteilung Deine Nachricht wurde erfolgreich gesendet! Dein Name Deine Telefon-Nummer contactform.looks-awesome.com http://looks-awesome.com 