<?php
/**
 * Template Name: Caución Send
 *
 */

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
ob_end_flush();

$datos = $_POST['datos'];
$dat = str_replace('\\', '', $datos);
$new = json_decode($dat, true);
$email = $new['email'];
$name = $new['nombre'];
$seguro = $new['seguro'];
$cotizacion_price = $new['cotizacion'];
$date = date('Y-m-d');
$cotizacion = substr($cotizacion_price, 0, strpos($cotizacion_price, "."));
$cotizacion = str_replace('$', "", $cotizacion_price);
$cotizacion = str_replace(',', "", $cotizacion);
$cotizacion = str_replace('.', "", $cotizacion);
$position = true;
$url = "";


if ($seguro!=""  && $email!="") {


    /// ADMINISTRADOR ///////
///
///
///
    $emailTo = 'comercial@foms.com.ar';

    //$emailTo = 'marcos@tomorrow.com.ar';
    $subject = 'Cotización Seguro ' . $name;
    
    
    foreach ($new as $key => $value) {
        
        $url .= "$key=$value&";
        
    }

    $url_completa= 'https://www.foms.com.ar/emails/otros-admin.php?'.$url;

    $body = url_get_contents($url_completa);


    $headers = 'From: ' . $name . ' <' . $emailTo . '>' . "\r\n" . 'Reply-To: ' . $email;

    wp_mail($emailTo, $subject, $body, $headers);
    $emailSent = true;




    /// USUARIO ///////
    $emailToUser = $email;
    if ($cotizacion < 30000) {
        $subject = '¡Estamos más cerca de darte la cotización de tu seguro de '. $seguro .'!';

    }else{
        $subject = '¡Ya tenemos la cotizacion para tu seguro de '. $seguro .'!';
    } 
    $body = '';

    foreach ($new as $key => $value) {
        
        $url .= "$key=$value&";
        
    }

    $url_completa= 'https://www.foms.com.ar/wp-content/themes/stockholm/emails/otros-usuario.php?'.$url;

    $body = url_get_contents($url_completa);

    $headers = 'From: ' . $name . ' <' . $emailToUser . '>' . "\r\n" . 'Reply-To: ' . $email;

    wp_mail($emailToUser, $subject, $body, $headers);
    $emailSent = true;

    //DB
    $body_clean = '';
    if ($name != '') {
        $body_clean = "$name te enviamos una copia de la solicitud que realizó en nuestro sitio web TEST https 2. \n\n\n\n";
    } else {
        $body_clean = "Te enviamos una copia de la solicitud que realizó en nuestro sitio web. \n\n\n\n";
    }
    if ($position) {
        if ($seguro == 'alquiler' && $cotizacion < 300) {
            $body_clean .= "<h3 style='color: #383a98;'>En breve un representante de nuestra compañía se pondrá en contacto con vos. \n\n\n\n</h3>";
        } else {
            if ($seguro == 'obra' || $seguro == 'servicios') {
                $body_clean .= "<h3 style='color: #383a98;'>La cotización para tu seguro es de $cotizacion_price Prima Trimestral (no incluye impuestos) \n\n\n\n</h3>";
            } else if ($seguro == 'sociedades') {
                $body_clean .= "<h3 style='color: #383a98;'>La cotización para tu seguro es de $cotizacion_price por socio asegurado \n\n\n\n</h3>";
            }else {
                $body_clean .= "<h3 style='color: #383a98;'>La cotización para tu seguro es de $cotizacion_price \n\n\n\n</h3>";
            }
            $body_clean .= "<h3 style='color: #383a98;'>En breve un representante de nuestra compañía se pondrá en contacto con vos. \n\n\n\n</h3>";
        }
    } else {
        $body_clean .= "<h3 style='color: #383a98;'>En breve un representante de nuestra compañía se pondrá en contacto con vos. \n\n\n\n</h3>";
    }

    $body_clean .= "También podes llamarnos al 011 6009 5100 o escribirnos a comercial@foms.com.ar \n\n\n\n";
    $body_clean .= "<hr> \n\n\n\n";
    $body_clean .= "<small>Esta cotización se realizó en base a la solicitud completada por $name el $date</small>. \n\n\n\n";
    ;
    if ($cotizacion < 300) {
        unset($new['cotizacion']);
    }

    foreach ($new as $key => $value) {
        $key = ucwords(str_replace('_', ' ', $key));
        $body_clean .= "$key: $value \n\n";

    }


    $post_arr = array(
        'post_title' => $name.' - '. $email . ' - '. $seguro . ' - '. $alquiler,
        'post_type' => 'caucion',
        'post_content' => $body_clean,
        'post_status' => 'publish',
        'post_author' => get_current_user_id(),
    );
    wp_insert_post($post_arr);

    /// -- BD



    if (isset($emailSent) && $emailSent == true) {
        $return = 'ok';

    } else {
        $return = 'no';
    }
}else {
    $return = 'no';
}

echo json_encode($return);